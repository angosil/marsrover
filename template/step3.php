<?php
echo "Third step: let's go to move your rover."."\n";
echo "For move the rover we need a command set."."\n";
echo "Easy: U(u) move up, D(d) move down, L(l) rotate to left, and R(r) rotate to right."."\n";
echo "Important: the move depend of the direction of the rover."."\n";
echo "Example:  your rover is in '5 x 3, Direction: N' and you say 'ululdd' your rover move to '8 x 2, Direction: S'"."\n";