<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 14/11/16
 * Time: 12:23
 */

namespace app;

class rover
{
    private $map;
    private $posX;
    private $posY;
    private $dir;

    /**
     * rover constructor.
     * @param map $map
     * @param int $posX
     * @param int $posY
     * @param string $dir, (N, S, E, W)
     */
    public function __construct(map $map, $posX, $posY, $dir){
        $this->map = $map;
        if ($this->map->getX() >= $posX && $this->map->getY() >= $posY){
            $this->posX = (int)$posX;
            $this->posY = (int)$posY;
        }else{
            //make errors template, eje new error->set('001');
            return 'error';
        }
        if (in_array($dir,array('N', 'S', 'E', 'W'))){
            $this->dir = $dir;
        }else{
            //make errors template, eje error->set('002');
            return 'error';
        }
    }

    /**
     * @return int, the position of the rover in x coordinate
     */
    public function getPosX(){
        return $this->posX;
    }

    /**
     * @return int, the position of the rover in y coordinate
     */
    public function getPosY(){
        return $this->posY;
    }

    /**
     * @return string
     */
    public function getDir(){
        return $this->dir;
    }

    public function getPosition(){
        return $this->getPosX().' x '.$this->getPosY().', Direction: '.$this->getDir();
    }

    private function moveFromDirection($d){
        switch ($this->getDir()){
            case 'N':
                $this->posX = $this->posX+$d;
                if ($this->posX > $this->map->getX())
                    $this->posX = 0;
                break;
            case 'S':
                $this->posX = $this->posX-$d;
                if ($this->posX < 0)
                    $this->posX = $this->map->getX();
                break;
            case 'E':
                $this->posY = $this->posY+$d;
                if ($this->posY > $this->map->getY())
                    $this->posY = 0;
                break;
            case 'W':
                $this->posY = $this->posY-$d;
                if ($this->posY < 0)
                    $this->posX = $this->map->getY();
                break;
        }
    }

    private function rotateFromDirection($d){
        switch ($this->getDir()){
            case 'N':
                $this->dir = $d==1 ? 'W' : 'E';
                break;
            case 'S':
                $this->dir = $d==1 ? 'E' : 'W';
                break;
            case 'E':
                $this->dir = $d==1 ? 'N' : 'S';
                break;
            case 'W':
                $this->dir = $d==1 ? 'S' : 'N';
                break;
        }
    }

    /**
     * @param string $commands, commands to move the rover.
     */
    public function move($commands){
        $commands = str_split(strtoupper($commands));
        foreach ($commands as $command){
            switch ($command) {
                case 'U':
                    $this->moveFromDirection(1);
                    break;
                case 'D':
                    $this->moveFromDirection(-1);
                    break;
                case 'L':
                    $this->rotateFromDirection(1);
                    break;
                case 'R':
                    $this->rotateFromDirection(-1);
                    break;
                default:
                    //make errors template, eje error->set('003');
                    echo 'error';
            }
        }
    }
}