<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 14/11/16
 * Time: 11:50
 */

namespace app;


class map
{
    private $x;
    private $y;

    /**
     * @param $x int
     * @return bool
     */
    public function setX($x){
        $this->x = (int)$x;
        return is_int($this->x);
    }

    /**
     * @return int, the size in x coordinate of the map.
     */
    public function getX(){
        return $this->x;
    }

    /**
     * @param $y int
     * @return bool
     */
    public function setY($y){
        $this->y = (int)$y;
        return is_int($this->y);
    }

    /**
     * @return int, the size in y coordinate of the map.
     */
    public function getY(){
        return $this->y;
    }

    /**
     * @return string, represent the size of the map.
     */
    public function getSize(){
        return $this->getX().' x '.$this->gety();
    }
}