<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 14/11/16
 * Time: 11:59
 */

namespace test;

use app\map;

require_once ('app/map.php');

class mapTest extends \PHPUnit_Framework_TestCase
{
    protected $map;

    public function setUp(){
        $this->map = new map();
    }

    public function testGetSize(){
        $map = $this->map;
        $map->setX(10);
        $this->assertTrue($map->getX() === 10);
        $map->setY(10);
        $this->assertTrue($map->getY() === 10);
        $this->assertTrue($map->getSize() === '10 x 10');
    }
}
