<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 14/11/16
 * Time: 12:39
 */

namespace test;

use app\rover;
use app\map;

require_once ('app/rover.php');
require_once ('app/map.php');

class roverTest extends \PHPUnit_Framework_TestCase
{
    protected $rover;
    protected $map;

    public function setUp(){
        $this->map = new map();
        $this->map->setX(10);
        $this->map->setY(10);
        $this->rover = new rover($this->map,2,3,'N');
    }

    public function testGetSize(){
        $this->assertTrue($this->map->getSize() === '10 x 10');
        $this->assertTrue($this->rover->getPosition() === '2 x 3, Direction: N');
    }

    public function testMove(){
        $this->rover->move('uuu');
        $this->assertTrue($this->rover->getPosition() === '5 x 3, Direction: N');
        $this->rover->move('ululdd');
        $this->assertTrue($this->rover->getPosition() === '8 x 2, Direction: S');
        $this->rover->move('rruuu');
        $this->assertTrue($this->rover->getPosition() === '0 x 2, Direction: N');
    }
}
