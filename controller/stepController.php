<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 14/11/16
 * Time: 13:49
 */

namespace controller;

use app\map;
use app\rover;

class stepController
{
    private $map,$rover;

    public function stepOne(){
        require_once ('template/step1.php');
        $line = trim(fgets(STDIN)); // reads one line from STDIN
        $cord = explode("x", $line);
        $cord[0] = (int)$cord[0];
        $cord[1] = (int)$cord[1];
        if($cord[0].'x'.$cord[1] == $line){
            $this->map = new map();
            $this->map->setX($cord[0]);
            $this->map->setY($cord[1]);
            require ('template/mapSize.php');
            //next step
            return true;
        }
        //next step
        return false;
    }

    public function stepTwo(){
        require_once ('template/step2.php');
        $line = trim(fgets(STDIN)); // reads one line from STDIN
        $cord = explode("x", $line);
        $cord[0] = (int)$cord[0];
        $cord[1] = (int)$cord[1];
        if (!in_array($cord[2],array('N','S','E','W'))){
            $dir = $cord[2];
            include ('template/error/002.php');
            //next step
            return false;
        }else{
            $this->rover = new rover($this->map,$cord[0],$cord[1],$cord[2]);
            require ('template/roverPosition.php');
            //next step
            return true;
        }
        //next step
        return false;
    }

    public function stepThree(){
        require_once ('template/step3.php');
        $line = '';
        while (!in_array(strtolower($line),array('y','ya'))) {
            $line = trim(fgets(STDIN)); // reads one line from STDIN
            $this->rover->move($line);
            require ('template/roverPosition.php');
        }
        //next step
        return true;
    }
}