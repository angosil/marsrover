<?php
/**
 * Created by PhpStorm.
 * User: sunand
 * Date: 14/11/16
 * Time: 13:04
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);

spl_autoload_register(function ($nombre_clase) {
    if(file_exists( $nombre_clase.'.php'))
        require_once($nombre_clase.'.php');
    else
        require_once(str_replace('\\','/',$nombre_clase).'.php');
});

use app\map;
use controller\stepController;

// make a class template, and use template->get('start');
require_once ('template/start.php');

$line = strtolower(trim(fgets(STDIN))); // reads one line from STDIN
if($line == 'n' || $line == 'no' || $line == 'not'){
    exit;
}

//create map
//draw map
//next step
$next = false;
$step = new stepController();
while (!$next){
    $next = $step->stepOne();
}
//create rover,
//draw rover in map,
$next = false;
while (!$next){
    $next = $step->stepTwo();
}
//instruction to move rover,
//repead from step 2.
$next = false;
while (!$next){
    $next = $step->stepThree();
}
echo "OK..\n";